# docker-webtop-uml-java

## Why

This image, based on [docker-webtop](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-webtop), 
provides an UML tool ([Modelio Open Source](https://www.modelio.org)), [Eclipse IDE for Java Developers](https://www.eclipse.org) 
and a JDK 17.

## Details

- See available branches

